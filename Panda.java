public class Panda {
	public String name;
	public int age;
	public String martialArts;
	
	public void doMartialArts() {
		if (this.martialArts != null) {
			System.out.println(this.name + " is doing " + this.martialArts + '.');
		}
		else {
			System.out.println(this.name + " can't do Martial Arts.");
		}
	}
	public void greeting() {
		System.out.println("Hi, I'm " + this.name + " and I am " + this.age + '.');
	}
}