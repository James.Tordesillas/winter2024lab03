import java.util.Scanner;

public class VirtualPetApp {
	public static void main (String[] args) {
		Scanner reader = new Scanner(System.in);
		Panda[] embarrassment = new Panda[4];
		
		//Asks user input for all the panda entries inside the embarrassment
		for (int i = 0; i<embarrassment.length; i++) {
			embarrassment[i] = new Panda();
			
			// Asks for user Input for the Panda fields
			System.out.print("\n");
			System.out.println("Input your Panda's name!");
			embarrassment[i].name = reader.nextLine();
			System.out.println("Input your Panda's age!");
			embarrassment[i].age = Integer.parseInt(reader.nextLine());
			System.out.println("Input your Panda's Martial Arts! (Optional, click enter to skip)");
			embarrassment[i].martialArts = reader.nextLine();
		}
		//Prints the 3 fields of the last inputed Panda
		System.out.print("\n");
		System.out.println(embarrassment[embarrassment.length-1].name);
		System.out.println(embarrassment[embarrassment.length-1].age);
		System.out.println(embarrassment[embarrassment.length-1].martialArts);
		
		//Calls the 2 instance method from the Panda.java file
		System.out.print("\n");
		embarrassment[0].doMartialArts();
		embarrassment[0].greeting();
		
	}
}